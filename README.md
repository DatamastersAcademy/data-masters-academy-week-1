

## Intro to Data Science - Week 1 (Introduction to Python) 

In this week we will introduce ourselves to Python. 

---
## Class 1: Basic functions in Python 

The first class on Introduction to Python will cover: 

1. Data types
2. Numbers
3. Strings
4. Printing
5. Lists
6. Dictionaries
7. Booleans
8. Tuples
9. Sets
10. Comparison Operators
11. if, elif, else Statements
12. for Loops
13. while Loops
14. range()
15. list comprehension
16. functions
17. lambda expressions
18. map and filter
19. methods

---

## Class 2 : Numpy, Pandas & Matplotlib

The second class will be dedicated on understanding and using the most popular libraries in Python such as Numpy, Pandas and Matplotlib.
In this class we will cover: 

1. Numpy Arrays
2. Numpy Indexing and Selection 
3. Numpy Operations
4. Pandas Series 
5. Pandas Data Frames 
6. Inputs and Outputs
7. Graphs and plots in Matplotlib


---

## Class 3: Lab exercises: 

In this last class of this week we will solve exercises and problems together covering everything that has been learn before.
